#!/bin/bash
# compile C++ source
make

function python() {
    python3 $@
}

# pushd foo
# python ./gen.py
# popd

# gen symbol links
# for f in `ls foo/foo-*`; do
# 	ln -sf $f .
# done

EXE=mathindex.exe

function rerank() {
input=$1
cntl=$2
metric=$3
tput setaf 5 && echo "===  run reranker to generate HTML results page (METRIC=$metric) ===" && tput sgr0
#cat $input
python rerank_results.py $cntl $input $metric db-index/${cntl}-output-rerank_results.txt # -h ./db-index/${cntl}-html
#cat db-index/${cntl}-output-rerank_results.txt
}

function core_engine() {
CNTL=$1
cat $CNTL
tput setaf 5 && echo "===  run index.py ===" && tput sgr0
set -x

python index.py $CNTL | tee db-index/${CNTL}-stdout-index.py.txt
#read a

tput setaf 5 && echo "===  run query.py ===" && tput sgr0
python query.py $CNTL | tee db-index/${CNTL}-stdout-query.py.txt
#read a

tput setaf 5 && echo "===  run core indexer ===" && tput sgr0
cat db-index/${CNTL}_i_* | ./$EXE -o db-index/${CNTL}-a.idx 2>db-index/${CNTL}-stderr-exe-o.txt | tee db-index/${CNTL}-stdout-exe-o.txt
cat db-index/${CNTL}_i_*
#read a

tput setaf 5 && echo "===  run core search engine ===" && tput sgr0
cat db-index/${CNTL}_q_*.tsv | ./$EXE -i db-index/${CNTL}-a.idx -v 2>db-index/${CNTL}-stderr-exe-i.txt | tee db-index/${CNTL}-stdout-exe-i.txt
cat db-index/${CNTL}_q_*
#read a

if false; then
	rerank db-index/${CNTL}-stdout-exe-i.txt ${CNTL} 12
fi;

set +x

}

function combine() {
tput setaf 5 && echo "===  combine results ===" && tput sgr0
set -x
python combine_rankings.py db-index/combine-output.tsv db-index/combine.cache -r ${1} \
	db-index/${1}-stdout-exe-i.txt -r ${2} db-index/${2}-stdout-exe-i.txt -m SLT 12 -m OPT 12
set +x
}

function regression() {
tput setaf 5 && echo "===  regression on combined results ===" && tput sgr0
set -x
python regression_reranking.py db-index/combine-output.tsv $1 $2 db-index/regression-output.trec.txt db-index 'Reggresion_task0' 0 -1 0 SLT_MSS 1 SLT_EWU_Prec 2 SLT_EW_Rec 3 OPT_MSS 4 OPT_EWU_Prec 5 OPT_EW_Rec
set +x
}

function regression_conv() {
tput setaf 5 && echo "===  regression results conversion ===" && tput sgr0
set -x
python experiment_tools/TREC_format_to_tsv.py ./db-index/regression-output.trec.txt ./foo-qrels.dat $1 -1 ./db-index/regression-output.tsv
set +x
}

function clean() {
	tput setaf 5 && echo "===  clean db-index ===" && tput sgr0
	rm -rf db-index/
	mkdir db-index
	rm -f *.cache
}

function pipe_results_from_Azero() {
input=$1
core_engine foo-operator.cntl
python3 experiment_tools/TREC_format_to_tsv.py $input ./NTCIR12_MathWiki-qrels_judge.dat ./foo-operator.cntl -1 db-index/${input}.tsv
python3 rerank_results.py foo-operator.cntl db-index/${input}.tsv 12 db-index/${input}.rerank.tsv
python3 experiment_tools/convert_tsv_to_TREC_format.py foo-operator.cntl db-index/${input}.rerank.tsv db-index/${input}.rerank.trec runrunrun 0 -1
}

function pipe_results_to_Azero() {
# core_engine foo-operator.cntl
python3 experiment_tools/convert_tsv_to_TREC_format.py foo-operator.cntl db-index/foo-operator.cntl-stdout-exe-i.txt db-index/${input}.firststage.trec runrunrun 0 -1
}

# clean
# pipe_results_from_Azero full-professor.dat
# set -x
# ./trec_eval -l3 NTCIR12_MathWiki-qrels_judge.dat ./full-professor.dat
# ./trec_eval -l3 NTCIR12_MathWiki-qrels_judge.dat ./db-index/full-professor.dat.rerank.trec
# set +x

pipe_results_to_Azero
