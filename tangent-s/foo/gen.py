#!/usr/bin/python3
import os
import uuid
import sys
from random import *
from bs4 import BeautifulSoup
sys.path.append("..")
from TangentS.math.math_extractor import MathExtractor
from TangentS.math.symbol_tree import SymbolTree

def stringizer(form):
    operator, xmlpath = False, 'tmp-representation.xml'
    if form == 'opt':
        operator = True
        xmlpath = 'tmp-content.xml'
    f = open(xmlpath, 'r')
    xml = f.read()
    f.close()
    trees, n_error = MathExtractor.parse_from_xml(xml, 'tmp-1', operator)
    return trees[0].tostring()

def visualizer(expression, form):
    expression = expression.replace(" ", "")
    expression = expression.replace("\n", "")
    expression = expression.replace("\t", "")
    parser = SymbolTree.parse_from_slt
    if form == 'opt':
        parser = SymbolTree.parse_from_opt
    tree = parser(expression)
    tmpname = str(uuid.uuid4())
    tree.save_as_dot('/tmp/%s.dot' % tmpname)
    os.system("dot -Tpng /tmp/%s.dot > tmp-%s.png" % (tmpname, form))

def readfile(path):
    f=open(path, 'r')
    savestr=f.read()
    f.close()
    return savestr

def instancialize_template(templ_file, n):
    soup = BeautifulSoup(readfile('tmp-representation.xml'), 'xml')
    repr_str = soup.contents[0].contents[1]
    soup = BeautifulSoup(readfile('tmp-content.xml'), 'xml')
    cont_str = soup.contents[0].contents[0]
    # print(repr_str)
    # print(cont_str)
    templ = readfile(templ_file)
    templ = templ.replace('REPLACE_QUERY_NUM', str(n))
    templ = templ.replace('REPLACE_REPRESENTATION_MATHML', str(repr_str))
    templ = templ.replace('REPLACE_CONTENT_MATHML', str(cont_str))
    return templ

def latexml_process(tex):
    tex = tex.replace("\n", "")
    tex = tex.replace("\t", "")
    os.system("latexmlmath --presentationmathml=tmp-representation.xml '%s' " % tex)
    os.system("latexmlmath --contentmathml=tmp-content.xml             '%s' " % tex)
    os.system("sed -i '/<\/math>/c\<\/annotation-xml><\/math>' tmp-content.xml")
    os.system("sed -i '/<math /c\<math><annotation-xml encoding=\"MathML-Content\">' tmp-content.xml")

def gen_query_xml(query_list):
    query_xml_head = '''
<?xml version="1.0" encoding="UTF-8"?>
<topics xmlns="http://ntcir-math.nii.ac.jp/" xmlns:m="http://www.w3.org/1998/Math/MathML">
'''
    topics = ''
    os.system("rm -f foo-query_groups.csv")
    for idx, q in enumerate(query_list):
        print('generating query #', idx)
        os.system("echo 'foo_query-{}' >> foo-query_groups.csv".format(idx))
        latexml_process(q)
        topic = instancialize_template('topic.template.xml', idx)
        topics += topic + '\n'
    f=open('query.xml', 'w')
    f.write(query_xml_head + topics + '</topics>\n')
    f.close()

def gen_qrels(query_list, doc_list):
    os.system("rm -f foo-qrels.dat")
    for q_idx, q in enumerate(query_list):
        for d_idx, d in enumerate(doc_list):
            os.system("echo 'foo_query-{} xxx doc-{}:0 {}' >> foo-qrels.dat".
                format(q_idx, d_idx, round(random(), 2))
            )

def gen_doc(tex, n):
    latexml_process(tex)
    doc = instancialize_template('doc.template.html', n)
    f=open('doc-' + str(n) + '.html', 'w')
    f.write(doc + '\n')
    f.close()

def gen_doc_list(doc_list):
    os.system("rm -f doc-*.html")
    os.system("rm -f doclist.txt")
    for idx, c in enumerate(doc_list):
        print('generating doc #', idx)
        os.system("echo 'foo/doc-{}.html' >> doclist.txt".format(idx))
        gen_doc(c, idx)

def process_wildcard():
    print('processing wildcard ...')
    os.system("sed -i '/<mi>Q<\/mi>/c\<qvar>*1*<\/qvar>' query.xml")
    os.system("sed -i '/<mi>W<\/mi>/c\<qvar>*2*<\/qvar>' query.xml")

# layt_expr = stringizer('slt')
# optr_expr = stringizer('opt')
# print('layt_expr', layt_expr)
# print('optr_expr', optr_expr)
# visualizer(optr_expr, 'opt')
# visualizer(layt_expr, 'slt')

# query_list = ['a^2 + a', 'Q^2 + Q', 'Q^2 + W']
# doc_list = ['x^2 + y', 'a^2 + a', 'a^2 + b + a', 'a^2 + a + xy']

query_list = ['(\\frac 1 n + 1)^n', 'a + 1', 'Q + 1']
doc_list = ['\lim (\\frac 1 x + 1)^x', '1/2 + 1', 'x + 1']

#doc_list = ['x^2 + x + y^2 + x']
# query_list = ['y^2 + y']
# #query_list = ['a^2 + a', 'Q^2 + Q', 'Q^2 + W']
#doc_list = ['x^2 + x + y^2 + x']
#doc_list = ['a +b + \\frac x y']

# query_list = ['Q + 3']
# doc_list = ['\\frac 1 2 + 3']

gen_doc_list(doc_list)
gen_query_xml(query_list)
process_wildcard()
gen_qrels(query_list, doc_list)
